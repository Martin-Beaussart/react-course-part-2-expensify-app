# Hello there  
This repo is here for learning purposes only  
The next points shows learning stuff on Git and React  
  
  
# Git Commands  
  
**git --version  :** Show the current version of git installed  
**git status :** View the changes to your project code  
**git add :** Add files to staging area (git add . or git add * to add all file on current folder (exepted files named on .gitignore))  
**git commit -m "Initial comit" :** Creates a new commit with files from staging area with the message "Initial comit"  
**git log :** View recet commits  
**git remote add origin  *Url* :** Connect your local folder and file to a remote repository with a specifi url ( here the current url used for this repository for example : https://Martin-Beaussart@bitbucket.org/Martin-Beaussart/react-course-part-2-expensify-app.git )  
**git push -u origin main /// git push -u origin main :** Push your local changes on the master/main branch of the repository (pleas check how is named the origin branch before, by default it is main or master depending the git repo site you use)  
**git remote -v :** Show the url of the repo you are connected  
**git commit -am "message" :** Wil do git add and commit in one line (warning : only add all modified files)
  
### Git docs about this topic
*Git Book , the ultimate reference about learning Git : https://git-scm.com/book/en/v2*  
  
  
## SSH Keys for git use:  
  
**(on Linux/Mac command prompt or Git Bash on windows ) ls -a ~/.ssh :** Check if there is exiting ssh keys on your machine (user account)  
**ssh-keygen -t rsa -b 4096 -C "your_email@example.com"  :** Generate a new SSH Key  
**eval "$(ssh-agent -s)" :** Check if ssh agent is running, and if it is not running it will start the ssh agent  
**cd C:/Users/<User>/.ssh /// cd %userprofile%/.ssh :** Go to the directory where the keys have been generated (If you didn't touch to the default options on the keygen creating tool)  
**dir :** Check if the keys have been generated in the /.ssh folder (id_rsa as private key, id_rsa.pub as public key)  
**clip < id_rsa.pub :** Copy on clipboard the content of the ssh Key  
**CREATE A NEW SSH KEY ON YOU GIT REPO ACCOUNT (GitHub, Bitbucket, ...)** : Copy the content of of your key from the clipboard then paste it to the content of the key you're creating on your acount  
**ssh -T git@github.com /// ssh -T git@bitbucket.org :** Make a ssh secure connection to the Github or Bitbucket Server  
  
### Bitbucket docs about this topic  
  
*https://support.atlassian.com/bitbucket-cloud/docs/set-up-an-ssh-key/*  
*https://confluence.atlassian.com/bitbucketserver/ssh-user-keys-for-personal-use-776639793.html*  
*https://confluence.atlassian.com/bitbucketserver/creating-ssh-keys-776639788.html*   
  
  
## Change the remote URL to your repository ( change https connection to ssh connection )  
  
**git remote -v :** Show the url of the repo you are connected  
**git remote set-url origin git@bitbucket.org:tutorials/tutorials.git :** Change the url to the ssh connection established  
  
### Bitbucket docs about this topic  
*https://support.atlassian.com/bitbucket-cloud/docs/change-the-remote-url-to-your-repository/ : Bitbucket docs about this topic*
  
  
# Heroku 
  
**heroku login :** Connect to your Heroku account  
**heroku create exemple-of-project-with-unique-name :** Create a space on heroku to deploy an application (need a unique name)  
**git remote -v :** Show if git is corectly connected to heroku
**git push heroku master :** Push you work on the public production server on heroku  
**heroku open :** Open in the Browser you web app !!
**heroku logs :** Get errors log from the web app from heroku 

## Setting Environnement variable on heroku  
  
**heroku config :** Show current config of heroku and environnement variables
**heroku config:set KEY=value :** Set an environement variable KEY with value "value"
**heroku config unset:KEY** Unset the environnement variable KEY