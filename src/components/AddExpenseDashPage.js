import React from 'react'
import ExpenseForm from './ExpenseForm'
import { connect } from 'react-redux'
import {startAddExpense} from '../actions/expenses'

export class AddExpenseDashPage extends React.Component{
  
  render(){
    return(
      <div>
      <div className="page-header" >
        <div className="content-container" >
          <h1 className="page-header_title" >Add Expense</h1>
        </div>
      </div>
      <div>
      </div>
        <div className="content-container">
          <ExpenseForm 
          onSubmit={this.onSubmit} />
        </div>
      </div>
    )
  }

  onSubmit = (expense)=> {
    // props.dispatch(addExpense(expense))
    this.props.startAddExpense(expense)
    this.props.history.push('/dashboard')
    } 
}

const mapDispatchToprops = (dispatch) => (
  { startAddExpense: (expense) => dispatch(startAddExpense(expense))  
  })

export default connect(undefined, mapDispatchToprops)(AddExpenseDashPage)