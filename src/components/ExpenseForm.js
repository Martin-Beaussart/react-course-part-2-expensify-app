import React from 'react'
import moment from 'moment'
import { SingleDatePicker } from 'react-dates'

// const now = moment()
// console.log(now)
// console.log(now.format('MMM Do, YYYY'))

export default class ExpenseForm extends React.Component{
  constructor(props){
    super(props)

    this.state = {
      description : props.expense ? props.expense.description : '',
      note : props.expense ? props.expense.note : '',
      amount :  props.expense ? props.expense.amount : '',
      createdAt:  props.expense ? props.expense.createdAt : moment().valueOf(),
      calenderFocused: false,
      errorMessage: ''
    }

  }



  onDescriptionChange = (e) =>{
    const description = e.target.value;
    this.setState(()=> ({description:description}))
  }

  onTextAreaChange = (e) =>{
    //const note = e.target.value
    e.persist()
    this.setState(()=> ({note:e.target.value}))
  }

  onAmountChange = (e) => {
    const amount = e.target.value
    if(!amount || amount.match(/^\d*(\.\d{0,2})?$/)){
      this.setState(()=> ({amount:amount}))
    } else{

    }
  }

  onDateChange = (createdAt) =>{
    if(createdAt){
      this.setState(() => ({ createdAt:createdAt}))
    }
  }

  onCalendarFocusChange = ({focused}) =>{
    this.setState(()=>({calenderFocused : focused}))
  }

  onSubmit = (e) =>{
    e.preventDefault()

    if(!this.state.description || ! this.state.amount){
      const errorMessage = 'Pleas provide a description and an amount'
      this.setState(() => ({ errorMessage:errorMessage}))
    }else{
      this.setState(() => ({ errorMessage:'' }))
      this.props.onSubmit({
        description: this.state.description,
        amount: parseFloat(this.state.amount,10 ),
        createdAt: this.state.createdAt,
        note: this.state.note
      })
    }

  }

  render(){
    console.log(this.state.createdAt)
    return(
        <form onSubmit={this.onSubmit}
        className="form">
        { this.state.errorMessage && <p className="form__error" >{this.state.errorMessage}</p> }
          <input
          type="text"
          placeholder="Description"
          autoFocus
          value={this.state.description}
          onChange={this.onDescriptionChange}
          className="text-input"
          />
          <input
          type="text"
          placeholder="Amount"
          value={this.state.amount}
          onChange={this.onAmountChange}
          className="text-input"
          />
          <SingleDatePicker
          date={moment(this.state.createdAt)}
          onDateChange={this.onDateChange}
          focused={this.state.calenderFocused }
          onFocusChange={this.onCalendarFocusChange}
          numberOfMonths={1}
          isOutsideRange={()=> false}
          />
          <textarea 
          placeholder="Add a note for your expense (optional)"
          value={this.state.note}
          onChange={this.onTextAreaChange}
          className="textarea"
          />
          <div>
              <button className="bt-login"
              type="submit" >Set expense</button>
          </div>
        </form>
    )
  }
}