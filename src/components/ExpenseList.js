import React from 'react'
import { connect } from 'react-redux'
import ExpenseListitem from './ExpenseListItem'
import selectExpenses from '../selectors/expense'


const ExpenseList = (props) =>(
  <div className="content-container">
  <div>
    <div className="list-header">
      <div className="show-for-mobile">Expenses</div>
      <div className="show-for-desktop">Expense</div>
      <div className="show-for-desktop">Amount</div>
    </div>
  </div>
  <div className="list-body">
    {
      props.expenses.length === 0 ? ( 
        <div>
        <span className="list-item list-item--message"> No Expenses, sorry</span>
        </div>
        ) : (
        props.expenses.map((expense)=>{
          return <ExpenseListitem {...expense} key={expense.id}/>
        })
      )
    }
    </div>
  </div>
)

const mapStateToProps = (state)=>({ expenses : selectExpenses(state.expenses,state.filter)  })

export { ExpenseList }

export default connect(mapStateToProps)(ExpenseList)
