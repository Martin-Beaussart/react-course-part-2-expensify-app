import React from 'react'
import numeral from 'numeral'
import { connect } from 'react-redux' 
import selectExpenses from '../selectors/expense'
import { selectExpenseTotal } from '../selectors/expenses-total'
import { Link } from 'react-router-dom'

export class ExpensesSummaryComponent extends React.Component{
  render(){
    return(
      <div className="page-header">
        <div className="content-container">
          <h1 className="page-header__title" > Viewing    
            <span> {this.props.expenseCount} </span> 
            expenses totalling 
            <span> { numeral(this.props.expensesTotalPrice).format("$0,0.00") } </span> 
          </h1>
          <div className="page-header__actions">
            <Link className="bt-login" to="/create" > Add Expense </Link>
          </div>
        </div>
      </div>
    )
  }
}



const mapStateToProps = (state) => {
  const visibleExpenses = selectExpenses(state.expenses,state.filter)
  return{ 
    expenseCount : visibleExpenses.length, 
    expensesTotalPrice : selectExpenseTotal(visibleExpenses)
  }
}
export const ExpensesSummary =  connect(mapStateToProps)(ExpensesSummaryComponent) // Return the connected component to the store