import React from 'react'
import { connect } from 'react-redux'
import ExpenseForm from './ExpenseForm'
import { startEditExpense, startRemoveExpense } from '../actions/expenses'

export class EditExpensePage extends React.Component{

  startRemoveExpense = () =>{
    this.props.startRemoveExpense({ id: this.props.expense.id})
    this.props.history.push('/')
  }

  startEditExpense = (expense) =>{
    this.props.startEditExpense(this.props.expense.id, expense)
    //console.log(expense)
    this.props.history.push('/')

  }

  render(){
    return(
      <div>
        <div className="page-header" >
          <div className="content-container" >
            <h1 className="page-header_title" >Edit Expense</h1>
          </div>
        </div>
        <div className="content-container">
          <ExpenseForm 
          expense={this.props.expense}
          onSubmit={this.startEditExpense}/>
          <button onClick={this.startRemoveExpense} className="bt-login button--secondary">Remove Expense</button>
        </div>
      </div>
    )
  }


}

const mapStateToProps = (state, props) => ({
  expense: state.expenses.find((expense) => expense.id === props.match.params.id)
})

const mapDispatchToProps = (dispatch, props)=> (
  { 
    startRemoveExpense : (id) => dispatch(startRemoveExpense(id)),
    startEditExpense : (id,expense) => dispatch(startEditExpense(id,expense))
  })

export default connect(mapStateToProps, mapDispatchToProps)(EditExpensePage)