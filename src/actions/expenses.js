import uuid from 'uuid'
import database from '../firebase/firebase'

const ADD_EXPENSE = "ADD_EXPENSE"
const REMOVE_EXPENSE = "REMOVE_EXPENSE"
const EDIT_EXPENSE = "EDIT_EXPENSE"
const SET_EXPENSES = "SET_EXPENSES"

export const addExpense = (expense) => ({
  type: ADD_EXPENSE,
  expense
})

export const startAddExpense = (expenseData = {} ) => {
  return(dispatch, getState) => {
    const uid = getState().auth.uid
    const description = expenseData.description ? expenseData.description : ""
    const note = expenseData.note ? expenseData.note : ''
    const amount = expenseData.amount ? expenseData.amount : 0
    const createdAt = expenseData.createdAt ? expenseData.createdAt.toString() : 0
    
    const expense = { description, note, amount, createdAt }

    return database.ref(`users/${uid}/expenses`).push(expense).then((ref) => {
      dispatch(addExpense({
        id:ref.key,
        ...expense
      }))
    })
  }
}

export const removeExpense = ({ id } = {} ) => ({
  type: REMOVE_EXPENSE,
  id
})

export const startRemoveExpense = ({ id } ) => {
  return(dispatch) =>{
    const uid = getState().auth.uid;
    return database.ref(`users/${uid}/expenses/${id}`).remove().then( () => {
      dispatch(removeExpense({ id }))
    })
  }
}

export const editExpense = (id,updates) => ({
  type: EDIT_EXPENSE,
  id,
  updates
})

export const startEditExpense = (id, updates) =>{
  return (dispatch) =>{
    const uid = getState().auth.uid;
    return database.ref(`users/${uid}/expenses/${id}`).update(updates).then(() => {
      dispatch(editExpense(id,updates))
    })
  }
}

export const setExpenses = (expenses) => ({
  type: SET_EXPENSES,
  expenses
})

export const startSetExpenses = () => {
  return (dispatch, getState) => {
    const uid = getState().auth.uid
    const expenseList = []
    return database.ref(`users/${uid}/expenses`).once('value').then( (snapshot) => {
      snapshot.forEach( (childSnapshot) => { 
        console.log(snapshot);
        expenseList.push({ 
          ...childSnapshot.val(),
          id:childSnapshot.key
        })
      })
    dispatch(setExpenses(expenseList))

  })
}}