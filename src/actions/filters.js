
const SET_TEXT_FILTER = "SET_TEXT_FILTER"
const SET_SORT_BY_DATE = "SET_SORT_BY_DATE"
const SET_SORT_BY_AMOUNT = "SET_SORT_BY_AMOUNT"
const SET_START_DATE = "SET_START_DATE"
const SET_END_DATE = "SET_END_DATE"

export const setTextFilter = (text = '') =>({
  type: SET_TEXT_FILTER,
  text: text
})

export const setSortByDate = () => ({
type:SET_SORT_BY_DATE
})

export const setSortByAmount = () =>({
type:SET_SORT_BY_AMOUNT
})

export const setStartDate = (date = 125) => ({
  startDate:date,
  type:SET_START_DATE
})

export const setEndDate = (date = 1250) => ({
  endDate:date,
  type:SET_END_DATE
})