export const selectExpenseTotal = (expenses) =>{
let sum = 0
if(expenses){
    expenses.map((expense)=> sum += expense.amount )
    return sum
  }else return 0
}