import Enzyyme from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import DotEnv from 'dotenv'

Enzyyme.configure({
  adapter: new Adapter()
})

DotEnv.config({ path: '.env.test'})