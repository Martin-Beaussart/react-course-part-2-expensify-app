import selectExpenses from '../../selectors/expense'
import moment from 'moment'
import expenses from '../fixtures/expense.js'

test("Should filter by text value",()=>{
  const filter = {
    text:'a',
    sortBy:'text',
    startDate: undefined,
    endDate: undefined
  }
  const action = selectExpenses(expenses,filter)
  expect(action).toEqual([expenses[1],expenses[2]])
})

test("Should filter by startDate",()=>{
  const filter = {
    text:'',
    sortBy:'date',
    startDate: moment(0),
    endDate: undefined
  }
  const action = selectExpenses(expenses,filter)
  expect(action).toEqual([expenses[2],expenses[0]])
})


test("Should filter by endDate",()=>{
  const filter = {
    text:'',
    sortBy:'date',
    startDate: undefined,
    endDate: moment(0)
  }
  const action = selectExpenses(expenses,filter)
  expect(action).toEqual([expenses[0],expenses[1]])
})

test("Should filter by date",()=>{
  const filter = {
    text:'',
    sortBy:'date',
    startDate: undefined,
    endDate: undefined
  }
  const action = selectExpenses(expenses,filter)
  expect(action).toEqual([expenses[2],expenses[0],expenses[1]])
})

test("Should filter by amount",()=>{
  const filter = {
    text:'',
    sortBy:'amount',
    startDate: undefined,
    endDate: undefined
  }
  const action = selectExpenses(expenses,filter)
  expect(action).toEqual([expenses[1],expenses[0],expenses[2]])
})