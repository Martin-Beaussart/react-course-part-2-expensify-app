import {selectExpenseTotal} from '../../selectors/expenses-total'
import expenses from '../fixtures/expense'


test('Should return 0 if not expense', ()=>{
  expect(selectExpenseTotal([])).toBe(0)
})

test('Should correctly add up a single expense', () =>{
  expect(selectExpenseTotal([expenses[1]])).toBe(1000)
})

test('Should correctly add up a multiple expenses', () =>{
  expect(selectExpenseTotal(expenses)).toBe(1220)
})