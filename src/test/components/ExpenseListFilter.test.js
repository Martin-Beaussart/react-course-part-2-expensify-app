import React from 'react'
import { shallow } from 'enzyme'
import { ExpenseListFilter } from '../../components/ExpenseListFilter'
import { altFilter, filter } from '../fixtures/filter'
import moment from 'moment'

let setTextFilter, setSortByAmount, setSortByDate, setStartDate, setEndDate, wrapper

beforeEach(()=>{
  setTextFilter = jest.fn()
  setSortByAmount = jest.fn()
  setSortByDate = jest.fn()
  setStartDate = jest.fn()
  setEndDate = jest.fn()

  wrapper = shallow(
    <ExpenseListFilter
      setTextFilter={setTextFilter}
      setSortByAmount={setSortByAmount}
      setSortByDate={setSortByDate}
      setStartDate={setStartDate}
      setEndDate={setEndDate}
      filter={filter}
      
      />)
})

test('Should render ExpenseListFilter correctly ', () => {
  expect(wrapper).toMatchSnapshot()
})



test('Should render ExpenseListFilter with alternative filet data correctly ', () => {
  wrapper.setProps({
    filter: altFilter
  })
  expect(wrapper).toMatchSnapshot()
})


test('Should handle text change ', () => {
  const value = 'rent'
  wrapper.find('input').simulate('change', {
    target: {value: value }
  })
  expect(setTextFilter).toHaveBeenLastCalledWith(value)
})

test('Should handle set sort by date change', () => {
  const value = 'date'
  wrapper.find('select').simulate('change', {
    target: {value: value }
  })
  expect(setSortByDate).toHaveBeenCalled()
})

test('Should handle set sort by amount', () => {
  const value = 'amount'
  wrapper.find('select').simulate('change', {
    target: {value: value }
  })
  expect(setSortByAmount).toHaveBeenCalled()
})

test('Should handle date chnages', () => {
  
    const startDate = moment().add(4,'years')
    const endDate = moment().subtract(8,'years')
  
  wrapper.find('DateRangePicker').prop('onDatesChange')({startDate, endDate})
  expect(setStartDate).toHaveBeenLastCalledWith(startDate)
  expect(setEndDate).toHaveBeenLastCalledWith(endDate)
})

test('Should handle date focus changes', () => {
  const calandarFocused = 'endDate'
  wrapper.find('DateRangePicker').prop('onFocusChange')(calandarFocused)
  expect(wrapper.state('calenderFocused')).toBe(calandarFocused)
})


