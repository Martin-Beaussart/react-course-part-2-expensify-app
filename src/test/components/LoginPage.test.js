import React from 'react'
import { shallow } from 'enzyme'
import { LoginPageComponent } from '../../components/LoginPage'

test('Should render LoginPage component correctly',() =>{
  const wrapper = shallow(<LoginPageComponent />)
  expect(wrapper).toMatchSnapshot()
})

test('should start login on button click', () => {
  const startLogin = jest.fn()
  const wrapper = shallow(<LoginPageComponent startLogin={ startLogin}/> )
  wrapper.find('button').simulate('click')
  expect(startLogin).toHaveBeenCalled()
})