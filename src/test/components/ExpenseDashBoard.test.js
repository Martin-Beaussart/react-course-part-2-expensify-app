import React from 'react'
import { shallow } from 'enzyme'
import { ExpenseDashBoard } from '../../components/ExpenseDashBoard'

test("Should render ExpenseDashBoardPage correctly", () => {
  const wrapper = shallow(<ExpenseDashBoard />)
  expect(wrapper).toMatchSnapshot()
})