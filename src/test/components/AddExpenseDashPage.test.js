import React from 'react'
import { shallow } from 'enzyme'
import { AddExpenseDashPage } from '../../components/AddExpenseDashPage'
import expenses from '../fixtures/expense'

let startAddExpense, history, wrapper;

// https://jestjs.io/docs/en/api 
beforeEach( () => {
  startAddExpense = jest.fn()
  history = { push: jest.fn() }
  wrapper = shallow(<AddExpenseDashPage startAddExpense={startAddExpense} history={history} />)
})

test('should render AddExpensePage correctly', () => {
  expect(wrapper).toMatchSnapshot()
})

test('Should handle onSubmit',() =>{
  wrapper.find('ExpenseForm').prop('onSubmit')(expenses[0])
  expect(startAddExpense).toHaveBeenLastCalledWith(expenses[0])
  expect(history.push).toHaveBeenLastCalledWith('/dashboard')
})