import React from 'react'
import { shallow } from 'enzyme'
import { ExpensesSummaryComponent } from '../../components/ExpensesSummary'

test('should render ExpenseSummary correctly without expense', () =>{
  const wrapper = shallow(<ExpensesSummaryComponent expenseCount={0} expensesTotalPrice={0}/>)
  expect(wrapper).toMatchSnapshot()
})

test('should render ExpenseSummary correctly with one Expense', () =>{
  const wrapper = shallow(<ExpensesSummaryComponent expenseCount={1} expensesTotalPrice={125}/>)
  expect(wrapper).toMatchSnapshot()
})

test('should render ExpenseSummary correctly without multiple expense', () =>{
  const wrapper = shallow(<ExpensesSummaryComponent expenseCount={10} expensesTotalPrice={1250000}/>)
  expect(wrapper).toMatchSnapshot()
})
