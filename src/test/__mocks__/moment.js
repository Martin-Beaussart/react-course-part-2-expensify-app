//import moment from 'moment'
const moment = require.requireActual('moment')

export default (timestamp = 0) => { return moment(timestamp)}

// mockup that serve to change moment function for testing purposes (having everytime same moment value for being sure having the right values)