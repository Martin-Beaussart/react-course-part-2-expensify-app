import { addExpense,removeExpense,editExpense, startAddExpense, setExpenses, startSetExpenses, startRemoveExpense} from '../../actions/expenses'
import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'
import databse from '../../firebase/firebase'
import expenses from '../fixtures/expense'
import uuid from 'uuid'

const createMockStore = configureMockStore([thunk])

const ADD_EXPENSE = "ADD_EXPENSE"
const REMOVE_EXPENSE = "REMOVE_EXPENSE"
const EDIT_EXPENSE = "EDIT_EXPENSE"
const SET_EXPENSES = "SET_EXPENSES"

// beforeEach((done) =>{
//   const expensesData = {}
//   expenses.forEach( ({ id, description, note, amount, createdAt}) =>{
//     expensesData[id] = {description, note, amount, createdAt: createdAt.toString() }
//   })
//   databse.ref('expenses').set(expensesData).then( () => done())
// })

test("Should setup remove expense action object",()=>{
  const action = removeExpense({id: 45})
  expect(action).toEqual({
    type:REMOVE_EXPENSE,
    id:45
  })
})

// // test("Should setup remove expense action object",()=>{
// //   const action = removeExpense({id: 45})
// //   expect(action).toEqual({
// //     type:REMOVE_EXPENSE,
// //     id:45
// //   })
// // })

// // test('should remove expense from firebase', (done) => {
// //   const store = createMockStore({});
// //   const id = expenses[2].id;
// //   store.dispatch(startRemoveExpense({ id })).then(() => {
// //     const actions = store.getActions();
// //     expect(actions[0]).toEqual({
// //       type: 'REMOVE_EXPENSE',
// //       id
// //     });
// //     return database.ref(`expenses/${id}`).once('value');
// //   }).then((snapshot) => {
// //     expect(snapshot.val()).toBeFalsy();
// //     done();
// //   });
// // });

// // test("Should setup edit expense action object",()=>{
// //   const action = editExpense(
// //     45,
// //     {
// //       objectParam1 : 'Coucou',
// //       objectParam2 : 250
// //     })

// //     expect(action).toEqual({
// //       id:45,
// //       type:EDIT_EXPENSE,
// //       updates : {
// //         objectParam1 : 'Coucou',
// //         objectParam2 : 250
// //       }
// //     })
// // })

// // test("Should setup add expense action object with provided values",() => {
// //   const data = {
// //     id : 'ert123',
// //     description: 'Rent bill',
// //     amount: 600,
// //     createdAt: 1000,
// //     note: "Bill of last month"
// //   }
// //   const dataEdited = addExpense(data)
// //   expect(dataEdited).toEqual({
// //     type : ADD_EXPENSE,
// //     expense : {
// //       ...data,
// //       id: expect.any(String)
// //     }
// //   })
// // })

// test('Should add expense to database and store', (done) => { // For asynchronous function, done is usefull => test wil wait that the code reach done()
//   const store = createMockStore( { } )
//   const expenseData={
//     description: 'Mouse',
//     amount: 3000,
//     note: 'this is better',
//     createdAt : "1000"
//   }
//   store.dispatch(startAddExpense(expenseData)).then( () => {
//     const actions = store.getActions()
//     expect(actions[0]).toEqual({
//       type: ADD_EXPENSE,
//       expense:{
//         id: expect.any(String),
//         ...expenseData
//       }
//     })
//     return databse.ref('expenses/'+actions[0].expense.id).once('value')
//   }).then((snapshot) => {
//       expect(snapshot.val()).toEqual(expenseData)
//       done()
//     })
// })
// test('Should add expense with default values to database and store', (done) => {
//   const store = createMockStore( { } )
//   const expenseDefaultData={
//     description: '',
//     amount: 0,
//     note: '',
//     createdAt : 0
//   }
//   store.dispatch(startAddExpense(expenseDefaultData)).then( () => {
//     const actions = store.getActions()
//     expect(actions[0]).toEqual({
//       type: ADD_EXPENSE,
//       expense:{
//         id: expect.any(String),
//         ...expenseDefaultData
//       }
//     })
//     return databse.ref('expenses/'+actions[0].expense.id).once('value')
//   }).then((snapshot) => {
//       expect(snapshot.val()).toEqual(expenseDefaultData)
//       done()
//     })
// })

// test('should setup set expenses action object with data', () => {
//   const action = setExpenses(expenses)
//   expect(action).toEqual({
//     type: SET_EXPENSES,
//     expenses
//    })
// })

// // WWWTTTFFFKKK
// // test('should fetch the expenses from firebase', (done) =>{
// //   const store = createMockStore({})
// //   store.dispatch(startSetExpenses()).then( () => {
// //     const actions = store.getActions()
// //     expect(actions[0]).toEqual({
// //       type: SET_EXPENSES,
// //       expenses
// //      })
// //      done()
// //   })
// // })
