import { setStartDate, setEndDate, setSortByAmount, setTextFilter, setSortByDate} from '../../actions/filters'
import moment from 'moment'

const SET_TEXT_FILTER = "SET_TEXT_FILTER"
const SET_SORT_BY_DATE = "SET_SORT_BY_DATE"
const SET_SORT_BY_AMOUNT = "SET_SORT_BY_AMOUNT"
const SET_START_DATE = "SET_START_DATE"
const SET_END_DATE = "SET_END_DATE"

test("Should setup StartDate action object ", ()=>{
  const action = setStartDate()
  expect(action).toEqual({
    startDate:125,
    type:SET_START_DATE
  })
})

test("Should setup EndDate action object", ()=>{
  const action = setEndDate()
  expect(action).toEqual({
    endDate:1250,
    type: SET_END_DATE
  })
})

test("should setup sortByAmount action object", () =>{
  expect(setSortByAmount()).toEqual({type:SET_SORT_BY_AMOUNT})
})

test("should setup sortByDate action object", () =>{
  expect(setSortByDate()).toEqual({type:SET_SORT_BY_DATE})
})

test(" Should setup SortTextFilter with params", ()=>{
  expect(setTextFilter('date')).toEqual({
    type:SET_TEXT_FILTER,
    text:'date'
  })
})
test(" Should setup SortTextFilter without params", ()=>{
  expect(setTextFilter()).toEqual({
    type:SET_TEXT_FILTER,
    text:''
  })
})