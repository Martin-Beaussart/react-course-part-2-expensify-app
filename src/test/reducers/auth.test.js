import { ExpensesSummaryComponent } from '../../components/ExpensesSummary'
import authReducer from '../../reducers/auth'

test(" should set uid for login", () => {
  const action = {
    type:'LOGIN',
    uid: 'abc123'
  }
  const state = authReducer({}, action)
  expect(state.uid).toBe(action.uid)
})

test('Should clear uid for logout', () => {
  const state = authReducer({ uid: 'anything'}, {type: 'LOGOUT'})
  expect(state).toEqual({})
})