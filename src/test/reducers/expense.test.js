import expenseReducer from '../../reducers/expenses'
import expense from '../../selectors/expense'
import expenses from '../fixtures/expense'
import moment from 'moment'
import { setExpenses } from '../../actions/expenses'

const ADD_EXPENSE = "ADD_EXPENSE"
const REMOVE_EXPENSE = "REMOVE_EXPENSE"
const EDIT_EXPENSE = "EDIT_EXPENSE"

test('should set default state', () =>{
  const state = expenseReducer(undefined, { type: '@@INIT'})
  expect(state).toEqual([])
})

test('should remove expense by id',()=>{
  const state = expenseReducer(expenses, { type:REMOVE_EXPENSE, id: expenses[0].id})
  expect(state).toEqual([expenses[1],expenses[2]])
})

test('should not remove expense if no id found',()=>{
  const state = expenseReducer(expenses, { type:REMOVE_EXPENSE, id: '-1'})
  expect(state).toEqual(expenses)
})

test('should add an expense',()=>{
  const expense = {
    id:'4',
    description : '',
    note:'',
    amount : 20000,
    createdAt: moment(0).add(40,'days').valueOf()
  }
  const state = expenseReducer(expenses, { type:ADD_EXPENSE,  expense: expense })
  expenses.push(expense)
  expect(state).toEqual(expenses)
})

test('should edit an expense',()=>{
  const expense = {
    id:'1',
    description : '',
    note:'',
    amount : 20000,
    createdAt: moment(0).add(40,'days').valueOf()
  }
  const state = expenseReducer(expenses, { type:EDIT_EXPENSE,  expense: expense })
  expense[0] = expense
  expect(state).toEqual(expenses)
})

test('should not edit an expense if expense id not found',()=>{
  const expense = {
    id:'-1',
    description : '',
    note:'',
    amount : 20000,
    createdAt: moment(0).add(40,'days').valueOf()
  }
  const state = expenseReducer(expenses, { type:EDIT_EXPENSE ,expense:expense})
  expect(state).toEqual(expenses)
})

test("Should set expenses" , ()  => {
  const action = setExpenses(expenses[1])
  const state = expenseReducer(expenses,action)
  expect(expenses[1]).toEqual(state)
})