import  filterReducer from '../../reducers/filters'
import moment from 'moment';

const SET_TEXT_FILTER = "SET_TEXT_FILTER"
const SET_SORT_BY_DATE = "SET_SORT_BY_DATE"
const SET_SORT_BY_AMOUNT = "SET_SORT_BY_AMOUNT"
const SET_START_DATE = "SET_START_DATE"
const SET_END_DATE = "SET_END_DATE"

test("Should setup default value", ()=>{
  const state = filterReducer(undefined, { type: '@@INIT'})
  expect(state).toEqual({
    text: '',
    sortBy : 'date',
    startDate: moment().startOf('month'),
    endDate: moment().endOf('month')
  })
})

test("Shoul set sortBy to amount", ()=>{
  const state = filterReducer(undefined, {type: SET_SORT_BY_AMOUNT})
  expect(state.sortBy).toBe('amount')
})

test("should test sortBy to date", ()=>{
  const state = filterReducer(undefined,{ type: SET_SORT_BY_DATE})
  expect(state).toEqual({
    text: '',
    sortBy : 'date',
    startDate: moment().startOf('month'),
    endDate: moment().endOf('month')
  })
})

test("should set text filter",()=>{
  const state = filterReducer(undefined, { text:'e', type:SET_TEXT_FILTER})
  expect(state).toEqual({
    text: 'e',
    sortBy : 'date',
    startDate: moment().startOf('month'),
    endDate: moment().endOf('month')
  })
})

test("should set startDate filter", ()=>{
  const state = filterReducer(undefined, { startDate: moment().add(4,'days'), type: SET_START_DATE})
  expect(state).toEqual({
    text: '',
    sortBy : 'date',
    startDate: moment().add(4,'days'),
    endDate: moment().endOf('month')
  })
})

test("should set startDate filter", ()=>{
  const state = filterReducer(undefined, { endDate: moment().subtract(4,'days'), type: SET_END_DATE})
  expect(state).toEqual({
    text: '',
    sortBy : 'date',
    startDate: moment().startOf('month'),
    endDate: moment().subtract(4,'days')
  })
})