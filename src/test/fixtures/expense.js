 import moment from 'moment'
 
 export default [{
  id:'1',
  description : 'Gum',
  note:'',
  amount : 200,
  createdAt: moment().valueOf()
},
{
  id:'2',
  description : 'Ball',
  note:'',
  amount : 1000,
  createdAt: moment().subtract(4,'days').valueOf()
},
{
  id:'3',
  description : 'Sand',
  note:'',
  amount : 20,
  createdAt: moment().add(4,'days').valueOf()
}]
