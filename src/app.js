import 'normalize.css/normalize.css'
import './styles/styles.scss'
import 'react-dates/lib/css/_datepicker.css'

import configureStore from './store/configureStore'
import ReactDOM from 'react-dom'
import React from'react' 
import AppRouter,  { history} from "./routers/AppRouter"
import { Provider } from 'react-redux' // allow to every component to access to the store
import expenseReducer from './reducers/expenses'
import expenseFilter from  './reducers/filters'
import { addExpense, editExpense, removeExpense, startSetExpenses} from './actions/expenses'
import { setEndDate,setSortByAmount,setSortByDate,setStartDate,setTextFilter} from './actions/filters'
import getVisibleExpense from './selectors/expense'
import expenses from './test/fixtures/expense'
import { firebase } from './firebase/firebase'
import { login, logout } from './actions/auth'
import { LoadingPage } from './components/LoadingPage'

const store = configureStore()


// store.dispatch(setTextFilter(''))
// store.dispatch(addExpense(expenses[0]))
// store.dispatch(addExpense(expenses[1]))
// store.dispatch(addExpense(expenses[2]))

console.log(store.getState())

const jsx = (
  <Provider store={store}>
    <AppRouter/>
  </Provider>
)

let appRoot = document.getElementById("app")

ReactDOM.render(< LoadingPage /> ,appRoot)


let hasRendered = false
const renderApp = () => {
  ReactDOM.render(jsx, appRoot)
  hasRendered = true
}


firebase.auth().onAuthStateChanged( (user) =>{
  if(user) {
    console.log("Connected")
    store.dispatch(login(user.uid))
    store.dispatch(startSetExpenses()).then( () => {
      renderApp()
      if(history.location.pathname === '/'){
        history.push('/dashboard')
      }
    })
  }
  else {
    store.dispatch(logout())
    renderApp()
    console.log("DIS connected")
  }
})

