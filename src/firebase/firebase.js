// import * as firebase from 'firebase'
import expenses from '../test/fixtures/expense'

import firebase from 'firebase'
import 'firebase/database'

  // Your web app's Firebase configuration
  var firebaseConfig = {
    apiKey: process.env.FIREBASE_API_KEY,
    authDomain: process.env.FIREBASE_AUTH_DOMAIN,
    databaseURL: process.env.FIREBASE_DATABASE_URL,
    projectId: process.env.FIREBASE_PROJECT_ID,
    storageBucket: process.env.FIREBASE_STORAGE_BUCKET,
    messagingSenderId: process.env.FIREBASE_MESSAGING_SENDER_ID,
    appId: process.env.FIREBASE_APP_ID
  };

  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);

const database = firebase.database()
const googleAuthProvider = new firebase.auth.GoogleAuthProvider()

export { googleAuthProvider, firebase, database as default}






/*
// remove all
// database.ref().remove() 

// value
database.ref('expenses').on('value', (snapshot) => {
  const expenseList = []
  snapshot.forEach( (childSnapshot) => {
    expenseList.push({
      ...childSnapshot.val(),
      id: childSnapshot.key
    })
  })
console.log(expenseList)
})

//child_removed
database.ref('expenses').on('child_removed', (snapshot) => {
  console.log(snapshot.key, snapshot.val());
})

//child_changed
database.ref('expenses').on('child_changed', (snapshot) => {
  console.log(snapshot.key, snapshot.val());
})

//child_added
database.ref('expenses').on('child_added', (snapshot) => {
  console.log(snapshot.key, snapshot.val());
})


// Setup all expenses on DB
// const expenseList = expenses
// expenseList.forEach( (expense) => {
// let obj = expense
// database.ref('expenses').push(obj) 
// })


//
//
// FIREBASE DOES NOT SUPPORT ARRAYS !!!!!!
//
//

// database.ref('notes').push({
//   title: 'To do',
//   body: 'go for a run'
// })



// database.ref('notes').set(notes)

// FETCHING DATA

// on is listening to data modifications (subscribe to database), and off is for unsubscribing to the DB
// database.ref().on('value', (snapshot) => {
//   console.log(snapshot.val());
// })

// setTimeout( () => {
//   database.ref('age').set(30)
// }, 3000 )

// setTimeout( () => {
//   database.ref().off()
// }, 3000 )

// setTimeout( () => {
//   database.ref('age').set(40)
// }, 3000 )

// database.ref().on( 'value',(snapshot) =>{
//   console.log( ` ${snapshot.val().name} is a ${snapshot.val().job.title} at ${snapshot.val().location.city}`);
// })
// database.ref('name').set('Mike')


// Once is doing the fethcing only once, do nothing more and do not listen to data
// database.ref('location/city').once('value')
// .then( (snapshot) => {
//   const val = snapshot.val()
//   console.log(val);
// })
// .catch( (error) => {
//  console.log('Console error', error)
// })


// Setting up new datas on DB
// database.ref().set({
//   name: 'Martin Beaussart',
//   age:22,
//   isSingle: false,
//   stressLevel : 6,
//   job: {
//     title: 'Software developer',
//     country: 'USA'
//   },
//   location: {
//     city: "Gaurain-Ramecroix",
//     contry : "Belgium"
//   }
// }).then( () => { // Asynchronous programing
//   console.log('Data is saved')
// }).catch( (err) => {
//   console.lof('this failed', err)
// })

// Updates suport promises (catch() and then())
// database.ref().update({
//   stressLevel: 9,
//   'job/company' : 'Amazon',
//   'location/city' : 'Seattle'
// })

// Updates can set up new datas
// database.ref().update( {
//   name: "Mike",
//   age: 29,
//   job: 'Software Developer',
//   'location/city': 'city'
// })

//
// REMOVE
// 

// database.ref('isSingle').set(null) // Setting up null a data is like removing it from database

// database.ref('age').remove().then( () =>{
//   console.log('Remove succes')
// }).catch( (e) => {
//   console.log('remove unseccessfull ',e)
// })




// database.ref('age').set(27)
// database.ref('location/city').set('Tournai')

// database.ref('attributes').set({
//   weight : 78,
//   height : 78
// }).then( () =>{   // SUCCESS
//   console.log("then")
// }).catch( (error) =>{ // UNSUCCESS
//   console.log("error",error)
// })

// console.log(" Here is a log message") */
