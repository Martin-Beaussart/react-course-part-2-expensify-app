import React from 'react'
import { Router, Route, Switch, Link, NavLink} from 'react-router-dom'
import createHistory from 'history/createBrowserHistory'
import AddExpenseDashPage from '../components/AddExpenseDashPage'
import EditExpensePage from '../components/EditExpensePage'
import ExpenseDashBoard from '../components/ExpenseDashBoard'
import { LoginPageConnectedComponent } from '../components/LoginPage'
import NotFoundPage from '../components/NotFoundPage'
import PrivateRoute from './PrivateRoute'
import PublicRoute from './PublicRoute'


export const history = createHistory()

const AppRouter = () => (
  // BrowserRouter have a preHistory in him, we can personalize our own Router with his own history
  <Router history={history} >
    <div>
      <Switch>
        <PublicRoute path='/' component={ LoginPageConnectedComponent} exact={true}/>
        <PrivateRoute path="/dashboard" component={ExpenseDashBoard} />
        <PrivateRoute path="/create" component={AddExpenseDashPage} />
        <PrivateRoute path="/edit/:id" component={EditExpensePage}/>
        <Route component={NotFoundPage}/>
      </Switch>
    </div>
  </Router>
)

export default AppRouter