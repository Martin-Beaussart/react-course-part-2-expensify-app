import moment from 'moment'
import { selectExpenseTotal } from '../selectors/expenses-total'
 
const expenses =  [{
 id:'1',
 description : 'Gum',
 note:'',
 amount : 200,
 createdAt: 0
},
{
 id:'2',
 description : 'Ball',
 note:'',
 amount : 1000,
 createdAt: moment(0).subtract(4,'days').valueOf()
},
{
 id:'3',
 description : 'Sand',
 note:'',
 amount : 20,
 createdAt: moment(0).add(4,'days').valueOf()
}]

console.log(selectExpenseTotal(expenses))