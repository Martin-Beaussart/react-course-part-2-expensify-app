// HOC / Higher Order Component : A component that render component 
// reuse code
// render hijacking
// Props manip
// Abstract state

import React from 'react'
import ReactDOM from 'react-dom'

const Info = (props) =>(
  <div>
    <h1>Info</h1>
    <p>The info is {props.info}</p>
  </div>
)

const withAdminWarning = (WrappedComponent) =>{
  return (props) => (
    <div>
        {props.isAdmin && <p>This is a rivate info</p> }
        <WrappedComponent {...props}/>
    </div>
  )
}

const requireAuthentification = (Component) =>{
  return (props) =>(
    <div>
    <Component {...props}/>
      {props.isAuthenticated && <p>Thi is a message for autheticated persons</p>}
    </div>
  )
}


const AdminInfo = withAdminWarning(Info)
const AuthInfo = requireAuthentification(Info)

// ReactDOM.render(<AdminInfo info=" WHAT A INFO "  isAdmin={true} />,document.getElementById("app"))
ReactDOM.render(<AuthInfo info=" WHAT A INFO "  isAuthenticated={false} />,document.getElementById("app"))