//
// OBJECT DESTRUCTURING
// 
/*
const person = {
  age : 42,
  location: {
    city : 'Phyladeplhia',
    temp : 13
  }
}

const { name: firstname = 'anonymous', age} = person
console.log(firstname, age)

const { city, temp : temperature } = person.location
console.log(city,temperature)

const book = {
  title: 'Ego is the Enemy',
  author:'Ryan Holiday',
  publisher: {
    name: 'Penguin'
  }
}

const { name: publisherName = "Self-Published"}  = book.publisher
console.log(publisherName)

*/
//
// ARRAY DESTRUCTURING
//
const address = ['rue',"ville","region","code postal"]

const [ street, , region, postalCode = "7530"] = address
console.log(street, region, postalCode)