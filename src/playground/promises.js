const promise = new Promise((resolve, reject) => {
  setTimeout(() => {
     // Only one resolve argument is return in this function
     reject("Throw an error")
     resolve({
       name : 'Martin' ,
       age : '22'
     })
  }, 1500 /* ms */ )
})

promise.then( (data) => {
  console.log('1',data)
}).catch((error) => {
  console.log("Error is log in catch", error)
})

