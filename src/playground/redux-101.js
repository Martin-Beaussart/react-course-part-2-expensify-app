import { createStore } from 'redux'

const INCREMENT = 'INCREMENT'
const DECREMENT = 'DECREMENT'
const SET = 'SET'
const RESET = 'RESET'

const incrementCount = (value) => {
  const { incrementValue = 1} = (typeof value === 'number') ? { incrementValue: value} : 1
  return{
    type:INCREMENT,
    incrementValue: incrementValue
  }
}

const decrementCount = (value) => {
  const {decrementValue = 1} = typeof value === 'number' ? {decrementValue: value} : 1
  return{
    type:DECREMENT,
    decrementValue: decrementValue
  }
}

const resetCount = () => ({ type: RESET })

const setCount = (value) => {
  const {setValue = 101} = typeof value === 'number' ? {setValue: value} : 101
  return{
    type:SET,
    setValue:setValue
  }
}


// Reducers
// 1. Reducers are pire functions
// 2. Never change state or action
//

const countReducer = () =>  (state = {count : 10},action) => {
  switch(action.type){
    case INCREMENT:
      return{count: state.count + action.incrementValue }
      break;

    case DECREMENT:
      return{ count: state.count - action.decrementValue }
      break;

    case RESET:
      return { count: 0 }
      break;

    case SET:
    return{ count: action.setValue}  
    break;

    default:
      break;
  }
  return state
}

const store = createStore(countReducer)

store.subscribe(() =>
{
  console.log(store.getState())
})

store.dispatch(incrementCount())
store.dispatch(incrementCount(10))
store.dispatch(resetCount())
store.dispatch(decrementCount())
store.dispatch(decrementCount(10))
store.dispatch(decrementCount())
store.dispatch(setCount(42))
store.dispatch(setCount())

