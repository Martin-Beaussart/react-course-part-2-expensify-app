const ADD_EXPENSE = "ADD_EXPENSE"
const REMOVE_EXPENSE = "REMOVE_EXPENSE"
const EDIT_EXPENSE = "EDIT_EXPENSE"
const SET_EXPENSES = "SET_EXPENSES"

const expensesReducerDefaultState = []

export default (state = expensesReducerDefaultState, action) =>{
  switch(action.type){
    case ADD_EXPENSE:
      //return state.concat(action.expense)
      return [...state, action.expense]
      break;

    case REMOVE_EXPENSE:
      return state.filter(({id}) => id !== action.id)
      break;

    case EDIT_EXPENSE:
      return state.map((expense)=> {
        if(expense.id === action.id ){
          return {
            ...expense,
            ...action.updates
          }
        } else return expense

      })
      break;

      case SET_EXPENSES:
        return action.expenses
      break;
    
    default: return state
  }
}