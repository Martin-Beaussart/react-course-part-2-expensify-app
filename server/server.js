const path = require('path')
const express = require('express')
const app = express()
const publicPath = path.join(__dirname, '..', 'public')
const port = process.env.PORT || 3000; // 3000 on local machine, or dynamically asigned by remote server public (Heroku)

app.use(express.static(publicPath))

app.get('*', (requestObject, responseObject) => {
  responseObject.sendFile(path.join(publicPath,'index.html'))
})

app.listen(port, ()=>{
  console.log("Server is up")
} )